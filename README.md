# Addressbook application

<details>
    <summary><strong>Task status</strong></summary>

- [x] Create a web app using Next JS
- [x] Create Express JS app for the API
- [x] Add test cases (In progress)
</details>

## API

API code is under server folder

### Demo API: https://addressbook-api.onrender.com

### Swagger UI: https://addressbook-api.onrender.com/api-docs

### Completed:

#### Addressbooks:

- GET /addressbooks - Get all addressbooks

#### Contacts:

- GET /contacts - Get all contacts
- GET /contacts?addressBookIds="Comma separated addressBookIds e.g. 1,2"

### In progress:

- POST /auth

## Web App

Frontend code is under **client** folder

### Demo Web app: https://addressbook-app.onrender.com/address-books
