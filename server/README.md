# API

## List of APIs

### Swagger UI: http://localhost:5000/api-docs

### Setup:

- Create a **.env** file and add the following value
  DATABASE_URL=postgres://abadmin:RLdiXtWadJRrZQQlj0gm4zok139OSV2o@dpg-cglt6a9mbg56g40dnar0-a.oregon-postgres.render.com/addressbookdb

- Run the following code

  ```
  cd server
  npm install
  npm run db
  npm run dev

  ```

- Access via http://localhost:5000

- Run tests

  ```
  npm run test

  ```
