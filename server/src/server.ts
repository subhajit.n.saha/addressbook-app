import type { Application, Request, Response } from 'express';
import type { Server } from 'http';

import express from 'express';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';

import { handleGlobalError } from './middlewares/custom-error.js';

import { swaggerDocInit } from './swagger/index.js';

import addressBookRouter from './address-books/route.js';
import contactRouter from './contacts/route.js';

const app: Application = express();

const port = process.env.PORT || 5000;

app.use(express.json());
app.use(cors());

// Swagger
const swaggerSpec = swaggerDocInit(port);

// Routes
app.use('/addressbooks', addressBookRouter);
app.use('/contacts', contactRouter);

app.get('/', (req: Request, res: Response) => {
  res.json({ status: 'Address book API is running.' });
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Global error handler
app.use(handleGlobalError);

app.use('*', (req: Request, res: Response) => {
  res.status(404).json({ message: 'Route not found.' });
});

const startServer = () => {
  const server: Server = app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`Address book app is running at port ${port}.`);
  });
  return server;
};

const stopServer = (message) => {
  appServer.close(() => {
    // tslint:disable-next-line:no-console
    console.log(`Address book API app is stopped due to ${message}.`);
    process.exit(0);
  });
};

const appServer = startServer();

['SIGINT', 'SIGTERM', 'unhandledRejection', 'uncaughtException'].forEach(
  (event) => {
    process.on(`${event}`, stopServer);
  }
);

export default app;
