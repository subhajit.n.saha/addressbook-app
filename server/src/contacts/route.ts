import { Router } from 'express';
import {
  createContact,
  getContacts,
  updateContact,
  deleteContact
} from './controller.js';

const router = Router();

/**
 * @openapi
 * paths:
 *   /contacts:
 *     get:
 *       description: Get all contacts.
 *       responses:
 *         200:
 *           description: OK
 *   /contacts?addressBookIds:
 *     get:
 *       description: Get all unique contacts from the address books.
 *       parameters:
 *         - name: addressBookIds
 *           in: query
 *           description: List of Addressbook Ids
 *           required: true
 *           schema:
 *             type: string
 *       responses:
 *         200:
 *           description: OK
 */
router.get('/', getContacts);
router.post('/', createContact);
router.post('/', updateContact);
router.post('/', deleteContact);

export default router;
