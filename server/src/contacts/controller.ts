import { PrismaClient } from '@prisma/client';
import type { Request, Response } from 'express';
import type { Contact, IContact } from './model.js';
import { ContactSchema } from './model.js';
import { AddressBookIdsSchema } from './../address-books/model.js';
import { findUniqueContactsByName } from './../utils/unique-contacts.js';

const prisma = new PrismaClient();

const createContact = async (req: Request, res: Response) => {
  try {
    const { name, phone, addressBookId }: Contact = ContactSchema.parse(
      req.body
    );
    const createdContact: IContact = await prisma.contact.create({
      data: { name, phone, addressBookId }
    });
    res.status(201).json(createdContact);
  } catch (error) {
    res.status(400).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const getUniqueContactsByAddressBookIds = async (addressBookIds) => {
  const lookupAddressBookIds = addressBookIds.map((addressBookId) =>
    Number(addressBookId)
  );
  return prisma.contact.findMany({
    include: { addressBook: true },
    where: {
      addressBookId: {
        in: lookupAddressBookIds
      }
    },
    orderBy: {
      name: 'asc'
    }
  });
};

/**
 * @swagger
 * /contacts:
 *   get:
 *     tags:
 *       - contacts
 *     summary: Get all contacts with addressbooks
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: OK
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/definitions/Contact'
 */
const getContacts = async (req: Request, res: Response) => {
  try {
    let contacts;

    if (
      typeof req.query.addressBookIds === 'string' &&
      req.query.addressBookIds.length > 0
    ) {
      let addressBookIds = req.query.addressBookIds.split(',');
      addressBookIds = AddressBookIdsSchema.parse(addressBookIds);
      contacts = await getUniqueContactsByAddressBookIds(addressBookIds);
      contacts = findUniqueContactsByName(contacts);
    } else {
      contacts = await prisma.contact.findMany();
    }
    res.status(200).json(contacts);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const updateContact = async (req: Request, res: Response) => {
  const contactId = Number(req.params.id);
  try {
    const { addressBookId, name, phone } = ContactSchema.parse(req.body);
    const updatedContact: IContact = await prisma.contact.update({
      where: { id: contactId },
      data: { addressBookId, name, phone }
    });
    res.status(200).json(updatedContact);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const deleteContact = async (req: Request, res: Response) => {
  const contactId = Number(req.params.id);
  try {
    const deletedContact: IContact = await prisma.contact.delete({
      where: { id: contactId }
    });
    res.status(200).json(deletedContact);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

export { createContact, getContacts, updateContact, deleteContact };
