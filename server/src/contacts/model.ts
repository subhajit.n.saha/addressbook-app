import { z } from 'zod';

const ContactSchema = z.object({
  name: z.string().min(2),
  phone: z.string().regex(/^\+?\d{10,14}$/),
  addressBookId: z.number()
});

type Contact = z.infer<typeof ContactSchema>;

interface IContact extends Contact {
  id: number;
}

export { ContactSchema, Contact, IContact };
