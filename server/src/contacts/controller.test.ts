import request from 'supertest';
import app from './../server.js';

describe('Contacts API', () => {
  it('should get all contacts', async () => {
    const res = await request(app).get('/contacts').expect(200);
    expect(res.body.length).toBeGreaterThan(1);
  });
});
