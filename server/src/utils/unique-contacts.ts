import { Contact } from '@prisma/client';

const findUniqueContactsByName = (contacts: Contact[]) => {
  const uniqueContacts: Contact[] = [];
  type Lookup = {
    contact: Contact;
    occurrence: number;
  };

  const LookupMap = new Map<string, Lookup>();

  contacts.forEach((contact) => {
    if (!LookupMap.has(contact.name)) {
      LookupMap.set(contact.name, { contact, occurrence: 1 });
    } else {
      const existingContact = LookupMap.get(contact.name);
      LookupMap.set(contact.name, {
        contact,
        occurrence: existingContact.occurrence + 1
      });
    }
  });

  for (const key of LookupMap.keys()) {
    const currentContact = LookupMap.get(key);
    if (currentContact.occurrence === 1) {
      uniqueContacts.push(currentContact.contact);
    }
  }
  return uniqueContacts;
};

export { findUniqueContactsByName };
