import { findUniqueContactsByName } from './unique-contacts.js';
import { contacts } from './../../prisma/seeds/contacts.js';

describe('findUniqueContactsByName', () => {
  it('returns unique contacts', () => {
    const today = new Date();
    const inputContacts = contacts.map((contact, index) => {
      return {
        ...contact,
        createdAt: today,
        updatedAt: today,
        id: index + 1
      };
    });
    const expected = [
      {
        id: 1,
        name: 'Bob',
        phone: '+917420472947',
        addressBookId: 1,
        createdAt: today,
        updatedAt: today
      },
      {
        id: 5,
        name: 'John',
        phone: '+917425472947',
        addressBookId: 2,
        createdAt: today,
        updatedAt: today
      }
    ];
    expect(findUniqueContactsByName(inputContacts)).toEqual(expected);
  });

  it('returns all contacts if no duplicate contact', () => {
    const today = new Date();
    const inputContacts = [
      {
        id: 1,
        name: 'Bob',
        phone: '+917420472947',
        addressBookId: 1,
        createdAt: today,
        updatedAt: today
      },
      {
        id: 5,
        name: 'John',
        phone: '+917425472947',
        addressBookId: 2,
        createdAt: today,
        updatedAt: today
      }
    ];
    expect(findUniqueContactsByName(inputContacts)).toEqual(inputContacts);
  });

  it('returns empty array if empty address book is passed', () => {
    expect(findUniqueContactsByName([])).toEqual([]);
  });
});
