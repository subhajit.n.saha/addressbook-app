import swaggerJsdoc from 'swagger-jsdoc';

const swaggerDocInit = (port) => {
  const isProdEnv = process.env.NODE_ENV === 'production';

  return swaggerJsdoc({
    swaggerDefinition: {
      openapi: '3.0.2',
      info: {
        title: 'Addressbook API',
        description: 'Addressbook API practice app.',
        version: '1.0.0'
      },
      servers: [
        {
          url: isProdEnv
            ? 'https://addressbook-api.onrender.com'
            : `http://localhost:${port}`
        }
      ]
    },
    apis: ['./src/**/route*.ts']
  });
};

export { swaggerDocInit };
