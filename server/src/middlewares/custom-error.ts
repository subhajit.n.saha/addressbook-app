import type { Request, Response, NextFunction } from 'express';

class CustomError extends Error {
  status?: number;

  constructor(message: string, status?: number) {
    super(message);
    this.status = status;
  }
}

const handleError = (
  err: CustomError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const status = err.status || 500;
  const error = err.message || 'Internal server error.';
  res.status(status).send({ error });
};

const handleGlobalError = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof CustomError) {
    handleError(err, req, res, next);
  } else {
    handleError(new CustomError('Internal server error.'), req, res, next);
  }
};

export { CustomError, handleError, handleGlobalError };
