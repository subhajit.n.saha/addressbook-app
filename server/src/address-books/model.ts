import { z } from 'zod';

const AddressBookSchema = z.object({
  name: z.string().min(3)
});

type AddressBook = z.infer<typeof AddressBookSchema>;

interface IAddressBook extends AddressBook {
  id: number;
}

const AddressBookIdsSchema = z.array(z.string().min(1));

export { AddressBookSchema, AddressBookIdsSchema, AddressBook, IAddressBook };
