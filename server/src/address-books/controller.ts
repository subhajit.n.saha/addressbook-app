import { PrismaClient } from '@prisma/client';
import type { Request, Response } from 'express';
import type { AddressBook, IAddressBook } from './model.js';
import { AddressBookSchema } from './model.js';

const prisma = new PrismaClient();

const createAddressBook = async (req: Request, res: Response) => {
  try {
    const { name }: AddressBook = AddressBookSchema.parse(req.body);
    const createdAddressBook: IAddressBook = await prisma.addressBook.create({
      data: { name }
    });
    res.status(201).json(createdAddressBook);
  } catch (error) {
    res.status(400).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const getAddressBooks = async (req: Request, res: Response) => {
  try {
    const addressBooks: IAddressBook[] = await prisma.addressBook.findMany({
      include: { contacts: true }
    });
    res.status(200).json(addressBooks);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const updateAddressBook = async (req: Request, res: Response) => {
  const addressBookId = Number(req.params.id);
  try {
    const { name } = AddressBookSchema.parse(req.body);
    const updatedAddressBook: IAddressBook = await prisma.addressBook.update({
      where: { id: addressBookId },
      data: { name }
    });
    res.status(200).json(updatedAddressBook);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

const deleteAddressBook = async (req: Request, res: Response) => {
  const addressBookId = Number(req.params.id);
  try {
    const deletedAddressBook: IAddressBook = await prisma.addressBook.delete({
      where: { id: addressBookId }
    });
    res.status(200).json(deletedAddressBook);
  } catch (error) {
    res.status(500).json({ error: error.message });
  } finally {
    await prisma.$disconnect();
  }
};

export {
  createAddressBook,
  getAddressBooks,
  updateAddressBook,
  deleteAddressBook
};
