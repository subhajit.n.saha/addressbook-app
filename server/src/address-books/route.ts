import { Router } from 'express';
import {
  createAddressBook,
  getAddressBooks,
  updateAddressBook,
  deleteAddressBook
} from './controller.js';

const router = Router();

/**
 * @openapi
 * /addressbooks:
 *   get:
 *     description: Get all addressbooks with contacts.
 *     responses:
 *       200:
 *         description: OK
 */
router.get('/', getAddressBooks);
router.post('/', createAddressBook);
router.post('/', updateAddressBook);
router.post('/', deleteAddressBook);

export default router;
