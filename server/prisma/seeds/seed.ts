import { PrismaClient } from '@prisma/client';
import { addressbooks } from './address-books.js';
import { contacts } from './contacts.js';

const prisma = new PrismaClient();

async function main() {
  const AddressBookRecords = await prisma.addressBook.createMany({
    data: addressbooks
  });

  const ContactRecords = await prisma.contact.createMany({
    data: contacts
  });

  return { AddressBookRecords, ContactRecords };
}

try {
  const { AddressBookRecords, ContactRecords } = await main();
  console.log({
    AddressBookRecords,
    ContactRecords
  });
} catch (error) {
  console.error(error);
} finally {
  await prisma.$disconnect();
}
