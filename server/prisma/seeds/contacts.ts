export const contacts = [
  {
    name: 'Bob',
    phone: '+917420472947',
    addressBookId: 1
  },
  {
    name: 'Mary',
    phone: '+917421472947',
    addressBookId: 1
  },
  {
    name: 'Jane',
    phone: '+917229372947',
    addressBookId: 1
  },
  {
    name: 'Mary',
    phone: '+917424472947',
    addressBookId: 2
  },
  {
    name: 'John',
    phone: '+917425472947',
    addressBookId: 2
  },
  {
    name: 'Jane',
    phone: '+917469472947',
    addressBookId: 2
  }
];
