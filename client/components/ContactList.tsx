import type { FC } from 'react';
import type { Contact } from '@/pages/address-books';

const ContactList: FC<{
  contacts: Contact[];
}> = ({ contacts }) => {
  return (
    <table className="w-full whitespace-nowrap">
      <thead className="bg-indigo-50">
        <tr className="focus:outline-none h-16 border border-gray-100 rounded">
          <th className="text-xs md:text-sm whitespace-normal">Sl. no.</th>
          <th className="text-xs md:text-sm whitespace-normal">Name</th>
          <th className="text-xs md:text-sm whitespace-normal">Phone number</th>
        </tr>
      </thead>
      <tbody>
        {contacts &&
          Array.isArray(contacts) &&
          contacts.map((contact, index) => {
            return (
              <tr
                key={`row-${index}`}
                className="focus:outline-none h-16 border border-gray-100 rounded"
              >
                <td className="text-center">{index + 1}</td>
                <td className="text-center">
                  <p className="text-xs md:text-sm">{contact.name}</p>
                </td>
                <td className="text-center">
                  <p className="text-xs md:text-sm">{contact.phone}</p>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default ContactList;
