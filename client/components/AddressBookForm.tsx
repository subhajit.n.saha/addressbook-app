import type { AddressBook } from '@/pages/address-books';
import Link from 'next/link';

const AddressBookForm = () => {
  return (
    <div className="m-7">
      <form action="" className="w-full md:w-1/2">
        <div className="mb-6">
          <label
            htmlFor="name"
            className="block mb-2 text-sm text-gray-600 dark:text-gray-400"
          >
            Address book name
          </label>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="Address book name"
            className="w-full px-5 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500"
          />
        </div>
        <div className="mb-6 flex items-center justify-left flex-wrap md:flex-row gap-3">
          <button
            type="button"
            className="px-5 py-3 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none"
          >
            Save
          </button>
          <Link href="/address-books">
            <button
              type="button"
              className="px-3 py-3 text-white bg-gray-500 rounded-md focus:bg-gray-600 focus:outline-none"
            >
              Cancel
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AddressBookForm;
