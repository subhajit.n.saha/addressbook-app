import type { FC } from 'react';

import Link from 'next/link';
import { useState } from 'react';
import type { AddressBook, Contact } from '@/pages/address-books';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

type AddressBookOption = {
  value: number;
  label: string;
  createdAt: Date;
  updatedAt: Date;
  contacts: Contact[];
};

class AddressBookData {
  addressBooks: AddressBook[] = [];

  constructor(data: AddressBookOption[]) {
    data.forEach((addressBookOption: AddressBookOption) => {
      const addressBook = {
        id: addressBookOption.value,
        name: addressBookOption.label,
        createdAt: addressBookOption.createdAt,
        updatedAt: addressBookOption.updatedAt,
        contacts: addressBookOption.contacts
      };
      this.addressBooks.push(addressBook);
    });
  }

  getAddressBookIds() {
    return this.addressBooks
      .map((addressBook: AddressBook) => addressBook.id)
      .join(',');
  }
}

const animatedComponents = makeAnimated();

const AddressBookFilter: FC<{
  addressBooks: AddressBook[];
}> = ({ addressBooks }) => {
  const [selectedAddressBook, setSelectedAddressBook] = useState<string | null>(
    null
  );

  const handleAddressBookSelection = (selectedAddressBookOption: any) => {
    if (selectedAddressBookOption) {
      const addressBook = new AddressBookData(selectedAddressBookOption);
      setSelectedAddressBook(addressBook.getAddressBookIds());
    }
  };

  const addressBookOptions = addressBooks.map((addressBook) => {
    return {
      value: addressBook.id,
      label: addressBook.name
    };
  });

  return (
    <div className="flex flex-wrap items-center justify-between gap-2">
      <p className="text-sm p-1">Select address books:</p>
      <Select
        className="p-0 sm:pl-2 w-64"
        components={animatedComponents}
        options={addressBookOptions}
        onChange={handleAddressBookSelection}
        isMulti
      />
      {selectedAddressBook && (
        <Link href={`/address-books/${selectedAddressBook}`}>
          <button className="focus:ring-2 focus:ring-offset-2 focus:ring-green-600 mt-4 sm:mt-0 inline-flex items-start justify-start px-6 py-3 bg-green-700 hover:bg-green-600 focus:outline-none rounded">
            <span className="text-sm font-medium leading-none text-white">
              View contacts
            </span>
          </button>
        </Link>
      )}
    </div>
  );
};

export default AddressBookFilter;
