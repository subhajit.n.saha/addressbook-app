# Addressbook web app

### Setup:

- Create a **.env.local** file and add the following value
  NEXT_PUBLIC_API_BASE_URL=http://localhost:5000/

- Run the following code

  ```
  cd client
  npm install
  npm run dev

  ```

- Access via http://localhost:3000/address-books

- Run tests

  ```
  npm run test

  ```
