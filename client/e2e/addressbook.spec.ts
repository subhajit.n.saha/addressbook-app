import { test, expect } from '@playwright/test';

test('should navigate to the address books page', async ({ page }) => {
  await page.goto('/');
  await page.click('text=About Page');
  await expect(page).toHaveURL('/about');
  await expect(page.locator('h1')).toContainText('About Page');
});
