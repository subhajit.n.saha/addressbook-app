import Head from 'next/head';
import SignInForm from '@/components/SignInForm';

export default function Home() {
  return (
    <>
      <Head>
        <title>Address book</title>
        <meta name="description" content="Address book application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://www.pwc.in/content/pwc/script/network/parallax/PwC-logo.svg"
          type="image/x-icon"
        />
      </Head>
      <main className="flex items-center min-h-screen bg-white dark:bg-gray-900">
        <SignInForm />
      </main>
    </>
  );
}
