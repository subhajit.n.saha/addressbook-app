import Head from 'next/head';

export default function Custom404() {
  return (
    <>
      <Head>
        <title>Address book: 404 error</title>
        <meta name="description" content="Address book application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://www.pwc.in/content/pwc/script/network/parallax/PwC-logo.svg"
          type="image/x-icon"
        />
      </Head>
      <main className="h-screen w-screen bg-gray-100 flex items-center">
        <div className="container flex flex-col md:flex-row items-center justify-center px-5 text-gray-700">
          <div className="max-w-md">
            <div className="text-5xl font-dark font-bold">
              <strong>Error: 404</strong>
              <p className="text-2xl md:text-3xl font-light leading-normal">
                <strong>Page not found.</strong>
              </p>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}
