import type { GetServerSideProps } from 'next';
import type { FC } from 'react';

import Head from 'next/head';
import Link from 'next/link';
import AddressBookFilter from '@/components/AddressBookFilter';
import ContactList from '@/components/ContactList';

export type Contact = {
  id: number;
  name: string;
  phone: string;
  addressBookId: number;
  createdAt: Date;
  updatedAt: Date;
};

export type ContactWithAddressBook = {
  id: number;
  name: string;
  phone: string;
  addressBookId: number;
  createdAt: Date;
  updatedAt: Date;
  addressBook: AddressBook;
};

export type AddressBook = {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  contacts: Contact[];
};

const fetchAddressBooks = async (): Promise<AddressBook[]> => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}addressbooks`
  );
  if (!response.ok) {
    throw new Error('Error while fetching address books');
  }
  const data = await response.json();
  return data;
};

const AddressBookPage: FC<{
  addressBooks: AddressBook[];
  contacts: Contact[];
}> = ({ addressBooks, contacts }) => {
  return (
    <>
      <Head>
        <title>Address book</title>
        <meta name="description" content="Address book application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://www.pwc.in/content/pwc/script/network/parallax/PwC-logo.svg"
          type="image/x-icon"
        />
      </Head>
      <main className="bg-gray-200 min-w-screen-sm">
        <section className="container px-3 md:px-10 py-2">
          <div className="py-2 md:py-7 flex items-center justify-between">
            <p
              tabIndex={0}
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              Contacts
            </p>
          </div>
          <div className="bg-white rounded px-3 md:px-10 py-2 md:py-10">
            <div className="flex items-center justify-between">
              <ul className="flex flex-wrap items-center justify-between w-full">
                <li className="flex items-center justify-between space-x-5">
                  <AddressBookFilter addressBooks={addressBooks} />
                </li>
                <li>
                  <Link href="/address-books/contacts/create">
                    <button className="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-600 mt-4 sm:mt-0 inline-flex items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded">
                      <span className="text-sm font-medium leading-none text-white">
                        Add contact
                      </span>
                    </button>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="mt-10">
              <p className="py-2 text-sm">Showing all contacts</p>
              <ContactList contacts={contacts} />
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<{
  addressBooks: AddressBook[];
}> = async () => {
  const addressBooks = await fetchAddressBooks();
  const contacts = addressBooks.reduce(
    (contacts: Contact[], addressbook: AddressBook) => {
      return contacts.concat(addressbook.contacts);
    },
    []
  );
  return {
    props: {
      addressBooks,
      contacts
    }
  };
};

export default AddressBookPage;
