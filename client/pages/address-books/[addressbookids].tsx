import type { GetServerSideProps } from 'next';
import type { FC } from 'react';
import type { ContactWithAddressBook } from '@/pages/address-books';

import Head from 'next/head';
import Link from 'next/link';
import ContactList from '@/components/ContactList';
import { z } from 'zod';

const AddressbookIdsSchema = z.object({
  addressbookids: z.string().min(1)
});

const fetchContacts = async (
  addressbookIds: string
): Promise<ContactWithAddressBook[]> => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}contacts?addressBookIds=${addressbookIds}`
  );
  if (!response.ok) {
    throw new Error('Error while fetching contacts');
  }
  const data = await response.json();
  return data;
};

const SelectedAddressBookPage: FC<{
  contacts: ContactWithAddressBook[];
  uniqueAddressBooks: string[];
}> = ({ contacts, uniqueAddressBooks }) => {
  return (
    <>
      <Head>
        <title>Address book - contact list</title>
        <meta name="description" content="Address book application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://www.pwc.in/content/pwc/script/network/parallax/PwC-logo.svg"
          type="image/x-icon"
        />
      </Head>
      <main className="bg-gray-200 min-w-screen-sm">
        <section className="container px-3 md:px-10 py-2">
          <div className="py-2 md:py-7 flex items-center justify-between">
            <p
              tabIndex={0}
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              Contacts
            </p>
          </div>
          <div className="bg-white rounded px-3 md:px-10 py-2 md:py-10">
            <div className="flex flex-wrap space-y-2 md:flex-row items-center justify-between pb-3">
              <div
                className="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3"
                role="alert"
              >
                <p className="text-sm font-bold">Selected address books:</p>
                <ul className="list-disc list-inside space-y-1">
                  {uniqueAddressBooks &&
                    Array.isArray(uniqueAddressBooks) &&
                    uniqueAddressBooks.map((addressBookName: string, index) => {
                      return (
                        <li className="text-xs" key={index}>
                          {addressBookName}
                        </li>
                      );
                    })}
                </ul>
              </div>
              <Link href="/address-books">
                <button
                  type="button"
                  className="px-3 py-3 text-white bg-gray-500 rounded-md focus:bg-gray-600 focus:outline-none"
                >
                  Address book
                </button>
              </Link>
            </div>
            <div>
              <ContactList contacts={contacts} />
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<{
  contacts: ContactWithAddressBook[];
}> = async ({ params }) => {
  let contacts: ContactWithAddressBook[] = [];
  let uniqueAddressBooks: string[] = [];

  try {
    const parsedData = AddressbookIdsSchema.parse(params);
    contacts = await fetchContacts(parsedData.addressbookids);
    const addressBookNames = contacts.map((contact: ContactWithAddressBook) => {
      return contact.addressBook.name;
    });
    uniqueAddressBooks = Array.from(new Set(addressBookNames));
  } catch (error) {
    console.error('Unable to parse addressbookids from query params');
  }

  return {
    props: {
      contacts,
      uniqueAddressBooks
    }
  };
};

export default SelectedAddressBookPage;
