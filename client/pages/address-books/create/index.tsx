import Head from 'next/head';
import type { FC } from 'react';
import AdddressBookForm from '@/components/AddressBookForm';

const AddressBookCreatePage: FC = () => {
  return (
    <>
      <Head>
        <title>Create a new address book</title>
        <meta name="description" content="Address book application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://www.pwc.in/content/pwc/script/network/parallax/PwC-logo.svg"
          type="image/x-icon"
        />
      </Head>
      <main className="bg-gray-200 min-w-screen-sm">
        <section className="container px-3 md:px-10 py-2">
          <h1 className="text-3xl pt-2 md:pt-10">Address book</h1>
          <p className="text-small pb-5">Create a new address book</p>
          <div className="bg-white rounded px-3 md:px-10 py-2 md:py-8">
            <div className="mt-10">
              <AdddressBookForm />
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export default AddressBookCreatePage;
